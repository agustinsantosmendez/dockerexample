package com.company.dockerexample;


import io.jmix.core.*;
import io.jmix.core.metamodel.model.MetaClass;
import io.jmix.core.metamodel.model.MetaProperty;
import io.jmix.core.metamodel.model.Range;
import io.jmix.core.security.SystemAuthenticator;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.stereotype.Component;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;


/**
 * Imports test data through JSON files in /data/seed /data/test directories
 *
 * The files should have the following naming convention:
 *
 * #nr#-#entityName#-#description#.zip
 *
 * - #nr# is just for ordering purposes
 * - #entityName# is should be the value of the @Entity annotation, where the '$' is replaced by a '_'
 *   - examples: 'Group', 'Customer'
 */
@Component("JsonDataImporter")
public class JsonDataImporter  {
    private final Logger log = LoggerFactory.getLogger(JsonDataImporter.class);

    public static String SEED_DATA_FILE_PATTERN = "data/seed/*.zip";

    @Autowired
    protected SystemAuthenticator authentication;

    @Autowired
    protected Resources resources;
    @Autowired
    protected Metadata metadata;
    @Autowired
    protected MetadataTools metadataTools;
    @Autowired
    protected EntityImportExport entityImportExport;
    @Autowired
    protected EntityImportPlans entityImportPlans;

    @EventListener
    public void applicationStarted(ApplicationStartedEvent ctxStartEvt) {
        importData(SEED_DATA_FILE_PATTERN);
    }

    protected void importData(String filePattern) {
        authentication.begin();
        try {
            try {
                log.info("Loading resources for: " + filePattern);
                Resource[] allZipResources = loadResources(filePattern);
                Arrays.sort(allZipResources, Comparator.comparing(Resource::getFilename));
                for (Resource resource:allZipResources) {
                    importDataForResource(resource);
                }
            } catch (IOException e) {
                log.error("Error", e);
            }
        } finally {
            authentication.end();
        }
    }

    Resource[] loadResources(String pattern) throws IOException {
        return ResourcePatternUtils.getResourcePatternResolver(resources).getResources(pattern);
    }

    protected void importDataForResource(Resource resource) throws IOException {
        byte[] zipBytes = IOUtils.toByteArray(resource.getInputStream());

        MetaClass entityClass = determineEntityClass(resource);
        if (entityClass != null) {
            Collection<Object> importedEntities = entityImportExport.importEntitiesFromZIP(zipBytes, createEntityImportPlan(entityClass));

            log.info("Import successful for: " + resource.getFilename() + ", number of entities: " + importedEntities.size());
        }
        else {
            log.warn("Class could not be found - import skipped for: " + resource.getFilename());
        }
    }
    protected MetaClass determineEntityClass(Resource resource) {
        String filename = resource.getFilename();
        MetaClass metaClass = null;
        if (filename != null) {
            String[] filenameParts = filename.split("-");
            String className = filenameParts[1];

            metaClass = metadata.getClass(className);
        }
        return metaClass;
    }

    protected EntityImportPlan createEntityImportPlan(MetaClass metaClass) {
        EntityImportPlanBuilder planBuilder = entityImportPlans.builder(metaClass.getJavaClass());

        for (MetaProperty metaProperty : metaClass.getProperties()) {
            if (!metadataTools.isJpa(metaProperty)) {
                continue;
            }

            switch (metaProperty.getType()) {
                case DATATYPE, ENUM -> planBuilder.addLocalProperty(metaProperty.getName());
                case EMBEDDED, ASSOCIATION, COMPOSITION -> {
                //case  ASSOCIATION, COMPOSITION -> {
                    Range.Cardinality cardinality = metaProperty.getRange().getCardinality();
                    if (cardinality == Range.Cardinality.ONE_TO_ONE) {
                        planBuilder.addOneToOneProperty(metaProperty.getName(), ReferenceImportBehaviour.IGNORE_MISSING);
                    }
/*                    if (cardinality == Range.Cardinality.MANY_TO_ONE) {
                        planBuilder.addManyToOneProperty(metaProperty.getName(), ReferenceImportBehaviour.IGNORE_MISSING);
                    }  else if (cardinality == Range.Cardinality.ONE_TO_MANY) {
                        planBuilder.addOneToManyProperty(metaProperty.getName(), null, CollectionImportPolicy.KEEP_ABSENT_ITEMS);
                    }*/
                }
                default -> throw new IllegalStateException("unknown property type");
            }
        }
        return planBuilder.build();
    }
}
